﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class AssetPreviewExtractor : Editor
{
    private const string PATH = "Assets/Resources/Thumbnails/";

    [MenuItem("Assets/AssetPreview/Save Asset Preview Image")]
    static void SaveAssetPreviewImage()
    {
        var obj = Selection.activeObject;
        var tex = AssetPreview.GetAssetPreview(obj);
        byte[] bytes = tex.EncodeToPNG();
        if (!Directory.Exists(PATH))
            Directory.CreateDirectory(PATH);
        File.WriteAllBytes($"{PATH}{obj.name}.png", bytes);
        AssetDatabase.Refresh();
    }
}
