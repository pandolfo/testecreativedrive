﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SceneObject : MonoBehaviour
{
    public string Name { get; set; }

    public Vector3 Position
    {
        get => transform.position;
        set => transform.position = value;
    }

    public Vector3 Rotation
    {
        get => transform.eulerAngles;
        set => transform.eulerAngles = value;
    }

    public Vector3 Scale
    {
        get => transform.localScale;
        set => transform.localScale = value;
    }

    private Color _color;
    public Color Color
    {
        get => _color;
        set
        {
            _color = value;
            foreach (var renderer in GetComponentsInChildren<Renderer>())
            {
                renderer.material.color = _color;
            }
        }
    }

    private Texture2D _texture;
    public Texture2D Texture
    {
        get => _texture;
        set
        {
            _texture = value;
            foreach (var renderer in GetComponentsInChildren<Renderer>())
            {
                renderer.material.mainTexture = _texture;
            }
        }
    }

    private Sprite _thumbnail;
    public Sprite Thumbnail
    {
        get
        {
            if (_thumbnail == null)
            {
                var tex = Resources.Load<Texture2D>($"Thumbnails/{Name}");
                _thumbnail =  Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
            }
            return _thumbnail;
        }
    }

    public Model ToJsonModel()
    {
        Model model = new Model();
        model.name = Name;
        model.Position = Position;
        model.Rotation = Rotation;
        model.Scale = Scale;

        return model;
    }

    private void Awake()
    {
        foreach (Transform child in transform)
        {
            GameObject obj = child.gameObject;
            var s = obj.GetComponent<Selectable>();
            if (s == null)
                s = obj.AddComponent<Selectable>();
            s.Parent = this;
        }
    }

    public void OnClick()
    {
        SceneManager.SelectedObject = this;
    }
}
