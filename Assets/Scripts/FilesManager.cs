﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

[System.Serializable]
public class Model
{
    public string name;
    [SerializeField]
    private float[] position;
    [SerializeField]
    private float[] rotation;
    [SerializeField]
    private float[] scale;

    public Vector3 Position
    {
        get => VectorFromArray(position);
        set => position = ArrayFromVector(value);
    }

    public Vector3 Rotation
    {
        get => VectorFromArray(rotation);
        set => rotation = ArrayFromVector(value);
    }

    public Vector3 Scale
    {
        get => VectorFromArray(scale);
        set => scale = ArrayFromVector(value);
    }

    private Vector3 VectorFromArray(float[] array)
    {
        return new Vector3(array[0], array[1], array[2]);
    }

    private float[] ArrayFromVector(Vector3 vector)
    {
        return new float[] { vector.x, vector.y, vector.z };
    }
}

[System.Serializable]
public class ModelsCollection
{
    public Model[] models;
}

public class FilesManager : MonoBehaviour
{
    public const string URL = "https://s3-sa-east-1.amazonaws.com/static-files-prod/unity3d/models.json";

    public static string DefaultDirectory { get => $"{Application.dataPath}/{DefaultResourcesDirectory}"; }
    public static string DefaultResourcesDirectory { get => "Data/"; }
    public static string DefaultFileName { get => "models"; }
    public static string DefaultFilePath { get => $"{DefaultDirectory}{DefaultFileName}"; }

    public void LoadFromURL(string url, Action<Model[]> callback)
    {
        StartCoroutine(GetJsonFromURL(url, (s) =>
        {
            ModelsCollection collection = GetModelsFromJson(s);
            callback?.Invoke(collection.models);
        }));
    }

    public void LoadFromFile(string path, Action<Model[]> callback)
    {
        string json = ReadJsonFromFile(path);
        ModelsCollection collection = GetModelsFromJson(json);
        callback?.Invoke(collection.models);
    }

    public void Save(List<SceneObject> objects, string path)
    {
        List<Model> models = new List<Model>();
        foreach (var obj in objects)
            models.Add(obj.ToJsonModel());

        ModelsCollection collection = new ModelsCollection();
        collection.models = models.ToArray();
        string json = JsonUtility.ToJson(collection, true);

        WriteJsonToFile(json, path);
    }

    public void Save(List<SceneObject> objects, string directory, string fileName, string extension)
    {
        if (!Directory.Exists(directory))
            Directory.CreateDirectory(directory);

        Save(objects, $"{directory}{fileName}{extension}");
    }

    private ModelsCollection GetModelsFromJson(string json)
    {
        return JsonUtility.FromJson<ModelsCollection>(json);
    }

    public string ConvertFromUtf8(byte[] bytes)
    {
        var enc = new UTF8Encoding(true);
        var preamble = enc.GetPreamble();
        if (preamble.Where((p, i) => p != bytes[i]).Any())
            return enc.GetString(bytes);
        return enc.GetString(bytes.Skip(preamble.Length).ToArray());
    }

    public IEnumerator GetJsonFromURL(string url, Action<string> callback)
    {
        UIManager.ShowLoading(true);

        UnityWebRequest request = UnityWebRequest.Get(url);
        yield return request.SendWebRequest();

        if (!request.isNetworkError && !request.isHttpError)
        {
            var json = ConvertFromUtf8(request.downloadHandler.data);
            UIManager.ShowLoading(false);
            callback?.Invoke(json);
        }
    }

    public string ReadJsonFromFile(string path)
    {
        string json = string.Empty;
        if (File.Exists(path))
            json = File.ReadAllText(path);

        return json;
    }

    public void WriteJsonToFile(string json, string path)
    {
        print($"Arquivo salvo em: {path}");

        if (!File.Exists(path))
        {
            using (FileStream fs = File.Create(path))
            {
                byte[] info = new UTF8Encoding(true).GetBytes(json);
                fs.Write(info, 0, info.Length);
            }
        }
        else
        {
            UIManager.OpenPopupYesNo("Deseja substituir o arquivo?", () => 
            {
                File.WriteAllText(path, json);
            });
        }

#if UNITY_EDITOR
        AssetDatabase.Refresh();
#endif
    }
}
