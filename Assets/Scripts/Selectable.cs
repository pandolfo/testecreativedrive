﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selectable : MonoBehaviour
{
    public SceneObject Parent { get; set; }

    /*private void OnMouseUpAsButton()
    {
        Parent.OnClick();
    }*/

    private void OnMouseDown()
    {
        Parent.OnClick();
        SceneManager.Instance.EditToolsManager.OnMouseDown(Parent);
    }

    private void OnMouseDrag()
    {
        SceneManager.Instance.EditToolsManager.OnDrag(Parent);
    }

    private void OnMouseUp()
    {
        SceneManager.Instance.EditToolsManager.OnMouseUp(Parent);
    }

}
