﻿using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    [SerializeField]
    private List<SceneObject> _objects;
    [SerializeField]
    private FilesManager _filesManager;

    [SerializeField]
    private EditToolsManager _editToolsManager;
    public EditToolsManager EditToolsManager => _editToolsManager;

    private static SceneObject _selectedObject;
    public static SceneObject SelectedObject 
    {
        get => _selectedObject;
        set
        {
            _selectedObject = value;
            UIManager.UIObjectProperties.UpdateProperties();
            UIManager.UIObjectsList.SelectToggle(_selectedObject);
        }
    }

    private static SceneManager _instance;
    public static SceneManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<SceneManager>();
            return _instance;
        }
    }

    private void Start()
    {
        var popup = UIManager.OpenPopup();
        popup.SetText("Carregar a cena de um arquivo remoto ou local?");
        popup.AddButton("Remoto", InitializeSceneFromURL);
        popup.AddButton("Local", UIManager.OpenLoadFilePopup);
    }

    public void InitializeSceneFromURL()
    {
        _filesManager.LoadFromURL(FilesManager.URL, (models) => 
        {
            foreach (var model in models)
            {
                InstantiateObject(model);
            }
        });
    }

    public void InitializeSceneFromFile()
    {
        InitializeSceneFromFile(FilesManager.DefaultFilePath);
    }

    public void InitializeSceneFromFile(string path)
    {
        _filesManager.LoadFromFile(path, (models) =>
        {
            foreach (var model in models)
            {
                InstantiateObject(model);
            }
        });
    }

    public SceneObject InstantiateObject(Model model)
    {
        SceneObject so = Instantiate(Resources.Load<GameObject>($"Models/{model.name}")).AddComponent<SceneObject>();
        so.Name = model.name;
        so.Position = model.Position;
        so.Rotation = model.Rotation;
        so.Scale = model.Scale;
        AddObject(so);
        return so;
    }

    public SceneObject InstantiateObject(SceneObject sceneObject)
    {
        return InstantiateObject(sceneObject.ToJsonModel());
    }

    private void AddObject(SceneObject obj)
    {
        _objects.Add(obj);
        UIManager.UIObjectsList.AddObjectToList(obj);
    }

    public void DeleteObject(SceneObject obj)
    {
        _objects.Remove(obj);
        UIManager.UIObjectsList.RemoveObjectFromList(obj);
        Destroy(obj.gameObject);
        UIManager.UIObjectProperties.UpdateProperties();
    }

    public void SaveScene()
    {
        UIManager.OpenInputPopup("Digite o nome do arquivo", FilesManager.DefaultFileName, (s) => 
        {
            _filesManager.Save(_objects, FilesManager.DefaultDirectory, s, ".json");
        });
        //_modelsManager.Save(_objects, _defaultPath);
    }

    public void LoadScene()
    {
        UIManager.OpenPopupYesNo("Se você carregar uma nova cena, as alterações não salvas serão perdidas. Continuar assim mesmo?", () => 
        { 
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex); 
        });
    }

    public void Quit()
    {
        UIManager.OpenPopupYesNo("Deseja sair? As alterações não salvas serão perdidas.", () => 
        {
            Application.Quit();
        });
    }
}
