﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EditMode
{
    None,
    Select,
    Move,
    Rotate,
    Scale
}

public class EditToolsManager : MonoBehaviour
{
    [SerializeField]
    private EditMode _editMode = EditMode.None;
    public EditMode EditMode => _editMode;

    [SerializeField]
    private float _dragDeadZone = 0.3f;

    private Vector3 _startDragMousePosition;
    private bool _isDragging;

    public void SetEditMode(EditMode mode)
    {
        _editMode = mode;
    }

    public void OnMouseDown(SceneObject obj)
    {        
        _startDragMousePosition = MouseWorldPosition(Input.mousePosition).Value;
    }

    public void OnDrag(SceneObject obj)
    {
        Vector3? mousePos = MouseWorldPosition(Input.mousePosition);

        if (mousePos == null)
            return;

        if (!_isDragging && Vector3.Distance(mousePos.Value, _startDragMousePosition) < _dragDeadZone)
            return;

        _isDragging = true;

        switch (_editMode)
        {
            case EditMode.None:
                break;
            case EditMode.Select:
                break;
            case EditMode.Move:
                if (mousePos != null)
                    obj.Position = MouseWorldPosition(Input.mousePosition).Value;
                break;
            case EditMode.Rotate:
                if (mousePos != null)
                {
                    var lookPos = mousePos.Value - obj.transform.position;
                    lookPos.y = 0;
                    var rot = Quaternion.LookRotation(lookPos);
                    obj.Rotation = rot.eulerAngles;
                }
                break;
            case EditMode.Scale:
                if (mousePos != null)
                    obj.Scale = Vector3.one * Vector3.Distance(_startDragMousePosition, mousePos.Value);
                break;
            default:
                break;
        }
    }

    public void OnMouseUp(SceneObject obj)
    {
        _isDragging = false;
        UIManager.UIObjectProperties.UpdateProperties();
    }

    private Vector3? MouseWorldPosition(Vector3 mousePosition)
    {
        var ray = Camera.main.ScreenPointToRay(mousePosition);
        int layerMask = 1 << 8;

        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layerMask))
        {
            return hit.point;
        }

        return null;
    }
}
