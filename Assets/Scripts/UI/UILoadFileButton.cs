﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILoadFileButton : MonoBehaviour
{
    private string _fileName;
    public string FileName
    {
        get => _fileName;
        set
        {
            _fileName = value;
            string[] split = _fileName.Split('/');
            NameText.text = split[split.Length - 1];
        }
    }

    private Text _nameText;
    public Text NameText
    {
        get
        {
            if (_nameText == null)
                _nameText = GetComponentInChildren<Text>();
            return _nameText;
        }
    }

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();
            return _button;
        }
    }

    private void Start()
    {
        Button.onClick.AddListener(() => 
        {
            SceneManager.Instance.InitializeSceneFromFile(FileName);
        });
    }
}
