﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UITextureButton : MonoBehaviour
{
    public Texture2D Texture;

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();
            return _button;
        }
    }

    private void Awake()
    {
        Button.onClick.AddListener(() => 
        {
            UIObjectProperties.SetTexture(Texture);
        });
    }
}
