﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UINewObjectButton : MonoBehaviour
{
    public Model Object { get; set; }

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();
            return _button;
        }
    }

    [SerializeField]
    private Image _image;
    public Image Image
    {
        get
        {
            if (_image == null)
                _image = GetComponentInChildren<Image>();
            return _image;
        }
    }

    public void Initialize(string name)
    {
        Object = new Model();
        Object.name = name;
        Object.Position = new Vector3();
        Object.Rotation = new Vector3();
        Object.Scale = new Vector3(1, 1, 1);

        var tex = Resources.Load<Texture2D>($"Thumbnails/{name}");
        Image.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));

        Button.onClick.AddListener(() => 
        {
            var obj = SceneManager.Instance.InstantiateObject(Object);
            SceneManager.SelectedObject = obj;
        });
    }
}
