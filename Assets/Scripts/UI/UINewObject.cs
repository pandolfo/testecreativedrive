﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UINewObject : UIOpenableToolbar
{
    [SerializeField]
    private Transform _buttonsContainer;
    [SerializeField]
    private UINewObjectButton ButtonPrefab;

    private List<UINewObjectButton> _buttons;

    private new void Start()
    {
        base.Start();

        GameObject[] objs = Resources.LoadAll<GameObject>("Models/");
        foreach (var obj in objs)
        {
            UINewObjectButton btn = Instantiate(ButtonPrefab, _buttonsContainer);
            btn.Initialize(obj.name);
        }
    }
}
