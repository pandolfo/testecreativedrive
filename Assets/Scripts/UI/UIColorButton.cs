﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIColorButton : MonoBehaviour
{
    public Color Color;

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();
            return _button;
        }
    }

    private void Start()
    {
        Button.onClick.AddListener(() =>
        {
            UIObjectProperties.SetColor(Color);
        });
    }
}
