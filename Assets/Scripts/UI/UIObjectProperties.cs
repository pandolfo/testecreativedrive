﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public class ObjectTransformUI
{
    public InputField XInputField;
    public InputField YInputField;
    public InputField ZInputField;

    public void Update(Vector3 transform)
    {
        XInputField.text = transform.x.ToString();
        YInputField.text = transform.y.ToString();
        ZInputField.text = transform.z.ToString();
    }

    public void SetCallbackX(Action<string> callback)
    {
        XInputField.onEndEdit.AddListener((s) => callback(s));
    }
    public void SetCallbackY(Action<string> callback)
    {
        YInputField.onEndEdit.AddListener((s) => callback(s));
    }
    public void SetCallbackZ(Action<string> callback)
    {
        ZInputField.onEndEdit.AddListener((s) => callback(s));
    }
}

[System.Serializable]
public class ObjectColorUI
{
    public UIColorButton ColorButton1;
    public UIColorButton ColorButton2;
    public UIColorButton ColorButton3;
}

[System.Serializable]
public class ObjectTextureUI
{
    public UITextureButton TextureButton1;
    public UITextureButton TextureButton2;
    public UITextureButton TextureButton3;
}

public class UIObjectProperties : MonoBehaviour
{
    [SerializeField]
    private Text _nameText;
    [SerializeField]
    private ObjectTransformUI _position;
    [SerializeField]
    private ObjectTransformUI _rotation;
    [SerializeField]
    private ObjectTransformUI _scale;
    [SerializeField]
    private ObjectColorUI _color;
    [SerializeField]
    private ObjectTextureUI _texture;
    [SerializeField]
    private Button _duplicateButton;
    [SerializeField]
    private Button _deleteButton;

    private void Awake()
    {
        _position.SetCallbackX((s) => SetPositionX(s));
        _position.SetCallbackY((s) => SetPositionY(s));
        _position.SetCallbackZ((s) => SetPositionZ(s));

        _rotation.SetCallbackX((s) => SetRotationX(s));
        _rotation.SetCallbackY((s) => SetRotationY(s));
        _rotation.SetCallbackZ((s) => SetRotationZ(s));

        _scale.SetCallbackX((s) => SetScaleX(s));
        _scale.SetCallbackY((s) => SetScaleY(s));
        _scale.SetCallbackZ((s) => SetScaleZ(s));

        _duplicateButton.onClick.AddListener(() => 
        {
            var so = SceneManager.SelectedObject;

            so = SceneManager.Instance.InstantiateObject(so);
            SceneManager.SelectedObject = so;
        });

        _deleteButton.onClick.AddListener(() =>
        {
            var so = SceneManager.SelectedObject;

            SceneManager.Instance.DeleteObject(so);
            SceneManager.SelectedObject = null;
        });
    }

    public void UpdateProperties()
    {
        var so = SceneManager.SelectedObject;

        gameObject.SetActive(so != null);

        if (!gameObject.activeSelf)
            return;

        _nameText.text = so.Name;
        _position.Update(so.Position);
        _rotation.Update(so.Rotation);
        _scale.Update(so.Scale);
    }

    public static void SetPositionX(string value)
    {
        var so = SceneManager.SelectedObject;
        so.Position = SetVectorX(value, so.Position);
    }

    public static void SetPositionY(string value)
    {
        var so = SceneManager.SelectedObject;
        so.Position = SetVectorY(value, so.Position);
    }

    public static void SetPositionZ(string value)
    {
        var so = SceneManager.SelectedObject;
        so.Position = SetVectorZ(value, so.Position);
    }

    public static void SetRotationX(string value)
    {
        var so = SceneManager.SelectedObject;
        so.Rotation = SetVectorX(value, so.Rotation);
    }

    public static void SetRotationY(string value)
    {
        var so = SceneManager.SelectedObject;
        so.Rotation = SetVectorY(value, so.Rotation);
    }

    public static void SetRotationZ(string value)
    {
        var so = SceneManager.SelectedObject;
        so.Rotation = SetVectorZ(value, so.Rotation);
    }

    public static void SetScaleX(string value)
    {
        var so = SceneManager.SelectedObject;
        so.Scale = SetVectorX(value, so.Scale);
    }

    public static void SetScaleY(string value)
    {
        var so = SceneManager.SelectedObject;
        so.Scale = SetVectorY(value, so.Scale);
    }

    public static void SetScaleZ(string value)
    {
        var so = SceneManager.SelectedObject;
        so.Scale = SetVectorZ(value, so.Scale);
    }

    private static Vector3 SetVectorX(string value, Vector3 vector)
    {
        float val = float.Parse(value);
        Vector3 v = vector;
        v.x = val;
        return v;
    }

    private static Vector3 SetVectorY(string value, Vector3 vector)
    {
        float val = float.Parse(value);
        Vector3 v = vector;
        v.y = val;
        return v;
    }
    private static Vector3 SetVectorZ(string value, Vector3 vector)
    {
        float val = float.Parse(value);
        Vector3 v = vector;
        v.z = val;
        return v;
    }

    public static void SetColor(Color color)
    {
        var so = SceneManager.SelectedObject;
        so.Color = color;
    }

    public static void SetTexture(Texture2D texture)
    {
        var so = SceneManager.SelectedObject;
        so.Texture = texture;
    }
}
