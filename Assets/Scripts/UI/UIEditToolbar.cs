﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEditToolbar : UIOpenableToolbar
{
    [SerializeField]
    private Toggle _selectToggle;
    [SerializeField]
    private Toggle _moveToggle;
    [SerializeField]
    private Toggle _rotateToggle;
    [SerializeField]
    private Toggle _scaleToggle;

    private void Awake()
    {
        _selectToggle.onValueChanged.AddListener((b) => 
        {
            if (b)
                SceneManager.Instance.EditToolsManager.SetEditMode(EditMode.Select);
        });

        _moveToggle.onValueChanged.AddListener((b) =>
        {
            if (b)
                SceneManager.Instance.EditToolsManager.SetEditMode(EditMode.Move);
        }); 

        _rotateToggle.onValueChanged.AddListener((b) =>
        {
            if (b)
                SceneManager.Instance.EditToolsManager.SetEditMode(EditMode.Rotate);
        });
        
        _scaleToggle.onValueChanged.AddListener((b) =>
        {
            if (b)
                SceneManager.Instance.EditToolsManager.SetEditMode(EditMode.Scale);
        });

        OnClose = () => _selectToggle.isOn = true;
    }
}
