﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIObjectsList : MonoBehaviour
{
    [SerializeField]
    private ScrollRect _scrollRect;

    [SerializeField]
    private UISceneObjectToggle _togglePrefab;

    private ToggleGroup _toggleGroup;

    private List<UISceneObjectToggle> _toggles = new List<UISceneObjectToggle>();

    private void Start()
    {
        _toggleGroup = gameObject.AddComponent<ToggleGroup>();
    }

    public void SelectToggle(SceneObject obj)
    {
        if (obj == null)
            return;

        var toggle = _toggles.Find((t) => ReferenceEquals(t.SceneObject, obj));
        toggle.Toggle.isOn = true;
    }

    public void AddObjectToList(SceneObject obj)
    {
        var toggle = Instantiate(_togglePrefab, _scrollRect.content);
        toggle.SceneObject = obj;
        toggle.Toggle.group = _toggleGroup;
        _toggles.Add(toggle);
    }

    public void RemoveObjectFromList(SceneObject obj)
    {
        var btn = _toggles.Find((b) => ReferenceEquals(b.SceneObject, obj));
        if (btn != null)
        {
            _toggles.Remove(btn);
            Destroy(btn.gameObject);
        }
    }
}
