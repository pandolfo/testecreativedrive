﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupInputField : UIPopup
{
    [SerializeField]
    private InputField _inputField;

    public void SetDefaultInput(string text)
    {
        _inputField.text = text;
    }

    public string GetInputFieldText()
    {
        return _inputField.text;
    }
}
