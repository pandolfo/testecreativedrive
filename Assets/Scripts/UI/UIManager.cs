﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _loadingText;

    [SerializeField]
    private UILoadFile _loadFilePopup;

    [SerializeField]
    private UIPopup _popupPrefab;
    public UIPopup PopupPrefab => _popupPrefab;

    [SerializeField]
    private UIPopupInputField _inputPopupPrefab;
    public UIPopupInputField InputPopupPrefab => _inputPopupPrefab;

    [SerializeField]
    private Button _popupButtonPrefab;
    public Button PopupButtonPrefab => _popupButtonPrefab;

    [SerializeField]
    private UIObjectProperties _uiObjectProperties;
    public static UIObjectProperties UIObjectProperties => Instance._uiObjectProperties;

    [SerializeField]
    private UIObjectsList _uiObjectsList;
    public static UIObjectsList UIObjectsList => Instance._uiObjectsList;

    private static UIManager _intance;
    public static UIManager Instance
    {
        get
        {
            if (_intance == null)
                _intance = FindObjectOfType<UIManager>();
            return _intance;
        }
    }

    public static void ShowLoading(bool isLoading)
    {
        Instance._loadingText.SetActive(isLoading);
    }

    public static void OpenLoadFilePopup()
    {
        Instance._loadFilePopup.Open();
    }

    public static void CloseLoadFilePopup()
    {
        Instance._loadFilePopup.Close();
    }

    public static UIPopup OpenPopup()
    {
        return Instantiate(Instance.PopupPrefab, Instance.transform);
    }

    public static UIPopup OpenPopup(string text, Action callback)
    {
        var popup = OpenPopup();
        popup.SetText(text);
        popup.AddButton("OK", callback);
        return popup;
    }

    public static UIPopup OpenPopupYesNo(string text, Action callbackYes)
    {
        var popup = OpenPopup();
        popup.SetText(text);
        popup.AddButton("Não", () => ClosePopup(popup));
        popup.AddButton("Sim", callbackYes);
        return popup;
    }

    public static UIPopupInputField OpenInputPopup(string text, string defaultInput, Action<string> callback)
    {
        var popup = Instantiate(Instance.InputPopupPrefab, Instance.transform);
        popup.SetText(text);
        popup.SetDefaultInput(defaultInput);
        popup.AddButton("Cancelar", () => ClosePopup(popup));
        popup.AddButton("Confirmar", () =>
        {
            string str = popup.GetInputFieldText();
            if (!string.IsNullOrEmpty(str)) ;
                callback?.Invoke(str);
        });
        return popup;
    }

    public static void ClosePopup(UIPopup popup)
    {
        popup.Close();
    }
}
