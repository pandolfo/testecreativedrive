﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIOpenableToolbar : MonoBehaviour
{
    [SerializeField]
    private Vector2 _openPosition;
    [SerializeField]
    private Vector2 _closedPosition;
    [SerializeField]
    private Button _openCloseButton;

    public Action OnOpen { get; set; }
    public Action OnClose { get; set; }

    private bool IsOpened => RectTransform.anchoredPosition == _openPosition;
    private RectTransform RectTransform => GetComponent<RectTransform>();

    protected void Start()
    {
        _openCloseButton.onClick.AddListener(() =>
        {
            if (IsOpened)
                Close();
            else
                Open();
        });
    }

    private void Open()
    {
        RectTransform.anchoredPosition = _openPosition;
        _openCloseButton.GetComponentInChildren<Text>().text = "Fechar";
        OnOpen?.Invoke();
    }

    private void Close()
    {
        RectTransform.anchoredPosition = _closedPosition;
        _openCloseButton.GetComponentInChildren<Text>().text = "Abrir";
        OnClose?.Invoke();
    }
}
