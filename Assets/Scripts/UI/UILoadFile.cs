﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UILoadFile : MonoBehaviour
{
    [SerializeField]
    private Transform _buttonsContainer;
    [SerializeField]
    private UILoadFileButton _buttonPrefab;
    [SerializeField]
    private Button _cancelButton;

    private List<UILoadFileButton> _buttons = new List<UILoadFileButton>();

    private void Start()
    {
        _cancelButton.onClick.AddListener(() => 
        {
            SceneManager.Instance.InitializeSceneFromURL();
            Close();
        });
    }

    public void Open()
    {
        if (GetFiles().Length == 0)
        {
            UIManager.OpenPopup("Nenhum arquivo local encontrado. Carregando arquivo remoto.", () => 
            {
                SceneManager.Instance.InitializeSceneFromURL();
                Close();
            });
        }

        RefreshButtons();

        gameObject.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    public void RefreshButtons()
    {
        foreach (var btn in _buttons)
            Destroy(btn.gameObject);

        _buttons = new List<UILoadFileButton>();

        var fileNames = GetFiles();
        foreach (var name in fileNames)
        {
            var btn = Instantiate(_buttonPrefab, _buttonsContainer);
            btn.FileName = name;
            btn.Button.onClick.AddListener(Close);
            _buttons.Add(btn);
        }
    }

    private string[] GetFiles()
    {
        if (!Directory.Exists(FilesManager.DefaultDirectory))
            return new string[0];
        return Directory.GetFiles(FilesManager.DefaultDirectory).Where(s => s.EndsWith(".json")).ToArray();
    }
}
