﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPopup : MonoBehaviour
{
    [SerializeField]
    private Text _text;
    [SerializeField]
    private RectTransform _buttonsContainer;

    public List<Button> Buttons { get; set; } = new List<Button>();

    public void Open()
    {
        gameObject.SetActive(true);
    }

    public void Close()
    {
        Destroy(gameObject);
    }

    public void SetText(string text)
    {
        _text.text = text;
    }

    public void AddButton(string text, Action callback)
    {
        Button btn = Instantiate(UIManager.Instance.PopupButtonPrefab, _buttonsContainer);
        btn.GetComponentInChildren<Text>().text = text;
        btn.onClick.AddListener(() =>
        {
            callback?.Invoke();
            Close();
        });
        Buttons.Add(btn);
    }

    private void RemoveButton(Button btn)
    {
        Buttons.Remove(btn);
        Destroy(btn.gameObject);
    }
}
