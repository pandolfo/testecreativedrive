﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISceneObjectToggle : MonoBehaviour
{
    [SerializeField]
    private Text _nameText;
    [SerializeField]
    private Image _image;

    private SceneObject _sceneObject;
    public SceneObject SceneObject 
    {
        get
        {
            return _sceneObject;
        }
        set
        {
            _sceneObject = value;
            _nameText.text = _sceneObject.Name;
            _image.sprite = _sceneObject.Thumbnail;
        }
    }

    private Toggle _toggle;
    public Toggle Toggle
    {
        get
        {
            if (_toggle == null)
                _toggle = GetComponent<Toggle>();
            return _toggle;
        }
    }

    private void Awake()
    {
        Toggle.onValueChanged.AddListener((b) =>
        {
            if (b)
                SceneManager.SelectedObject = SceneObject;
        });
    }
}
